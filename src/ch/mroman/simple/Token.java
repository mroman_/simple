package ch.mroman.simple;

import java.util.List;

public class Token implements Comparable {
	public final static int CHR = 4;
	public final static int DOUBLE = 1;
	public final static int INT = 0;
	public final static int LIST = 8;
	public final static int OPR = 2;

	public char c;

	public double d;
	public int i;
	public boolean isString = false;
	public List<Token> list;
	public char opr;
	public String string;

	public int type = -1;

	public Token(final char c) {
		this.type = Token.CHR;
		this.c = c;
	}

	public Token(final char opr, final boolean dummy) {
		this.type = Token.OPR;
		this.opr = opr;
	}

	public Token(final double d) {
		this.type = Token.DOUBLE;
		this.d = d;
	}

	public Token(final int i) {
		this.type = Token.INT;
		this.i = i;
	}

	public Token(final List<Token> list) {
		this.type = Token.LIST;
		this.list = list;
	}

	public Token(final List<Token> list, final String str) {
		this.type = Token.LIST;
		this.list = list;
		this.string = this.string;
		this.isString = true;
	}

	private int compareLists(final List<Token> a, final List<Token> b) {
		final int min = Math.min(a.size(), b.size());
		for (int i = 0; i < min; i++) {
			final int j = a.get(i).compareTo(b.get(i));
			if (j != 0)
				return j;
		}
		if (a.size() < b.size())
			return -1;
		else if (b.size() > a.size())
			return 1;
		else
			return 0;
	}

	public int compareTo(final Object o) {
		final Token t = (Token) o;
		final int i = new Integer(this.type).compareTo(t.type);
		if (i != 0)
			return i;
		else if (t.type == Token.INT)
			return new Integer(this.i).compareTo(t.i);
		else if (t.type == Token.DOUBLE)
			return new Double(this.d).compareTo(t.d);
		else if (t.type == Token.CHR)
			return new Integer(this.c).compareTo((int) t.c);
		else if (t.type == Token.OPR)
			return new Integer(this.opr).compareTo((int) t.opr);
		else if (t.type == Token.LIST)
			return this.compareLists(this.list, t.list);
		return 0;
	}

	public boolean isTrue() {
		if (this.type == Token.INT)
			return this.i != 0;
		if (this.type == Token.DOUBLE)
			return this.d != 0;
		if (this.type == Token.LIST)
			return this.list.size() != 0;
		return false;
	}

	@Override
	public String toString() {
		if (this.type == Token.INT)
			return Integer.toString(this.i);
		else if (this.type == Token.DOUBLE)
			return Double.toString(this.d);
		else if (this.type == Token.OPR)
			return String.valueOf(this.opr);
		else if (this.type == Token.CHR)
			return "'" + String.valueOf(this.c);
		else if (this.type == Token.LIST) {
			String s = "[";
			for (final Token t : this.list)
				s += t.toString() + " ";
			if (s.endsWith(" "))
				s = s.substring(0, s.length() - 1);
			s += "]";
			return s;
		}
		return "n/a:" + Integer.toString(this.type);
	}
}
