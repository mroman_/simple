package ch.mroman.simple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
	private final static Map<String, List<Token>> cache = new HashMap<String, List<Token>>();

	public static List<Token> staticAll(final String string) {
		synchronized (Lexer.cache) {
			if (Lexer.cache.get(string) != null)
				return Lexer.cache.get(string);
			return new Lexer(string).all();
		}
	}

	private String string;

	public Lexer(final String string) {
		this.string = string;
	}

	public List<Token> all() {
		synchronized (Lexer.cache) {
			/**
			 * Uses a cache because some shortcuts will be frequently parsed :)
			 */
			if (Lexer.cache.get(this.string) != null)
				return Lexer.cache.get(this.string);
			Token t;
			final List<Token> all = new ArrayList<Token>();
			while ((t = this.next()) != null)
				all.add(t);
			Lexer.cache.put(this.string, all);
			return all;
		}
	}

	public Token next() {
		/*
		 * Remove leading spaces
		 */
		while (this.string.startsWith(" "))
			this.string = this.string.substring(1);

		/*
		 * Regular expression patterns
		 * 
		 * ^(..)(.*)$ will give us what we want to match in group(1) and the
		 * rest of the string in group(2). Thus we set string = group(2) in
		 * order to remove the part of the string we just "lexed" so the next
		 * call to next() can continue "lexing".
		 */
		final Pattern pdbl = Pattern.compile("^(_?[0-9]+\\.[0-9]+)(.*)$");
		final Matcher mdbl = pdbl.matcher(this.string);
		final Pattern pint = Pattern.compile("^(_?[0-9]+)(.*)$");
		final Matcher mint = pint.matcher(this.string);
		final Pattern pstr = Pattern.compile("^(\\(.*?\\))(.*)$");
		final Matcher mstr = pstr.matcher(this.string);
		final Pattern pchr = Pattern.compile("^('.)(.*)$");
		final Matcher mchr = pchr.matcher(this.string);
		final Pattern popr = Pattern
				.compile("^([a-zA-Z]{1}|[\\p{Punct}]{1})(.*)$");
		final Matcher mopr = popr.matcher(this.string);

		/*
		 * We try to match the patterns against the string in a specific order.
		 */
		if (mdbl.find()) {
			final String part = mdbl.group(1);
			this.string = mdbl.group(2);
			return new Token(Double.parseDouble(part.replace('_', '-')));
		} else if (mint.find()) {
			final String part = mint.group(1);
			this.string = mint.group(2);
			return new Token(Integer.parseInt(part.replace('_', '-')));
		} else if (mstr.find()) {
			final String part = mstr.group(1);
			final List<Token> list = new ArrayList<Token>();
			this.string = mstr.group(2);

			for (final char c : part.toCharArray())
				list.add(new Token(c));

			return new Token(list, part);
		} else if (mchr.find()) {
			final String part = mchr.group(1);
			this.string = mchr.group(2);
			return new Token(part.charAt(1));
		} else if (mopr.find()) {
			final String part = mopr.group(1);
			this.string = mopr.group(2);
			return new Token(part.charAt(0), true);
		}

		return null;
	}
}
