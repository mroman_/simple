import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import ch.mroman.simple.Lexer;
import ch.mroman.simple.Token;

public class Simple {
	public static <T extends Comparable> void assertEquals(final T a, final T b) {
		if (a.compareTo(b) != 0)
			throw new RuntimeException("FAIL");
	}

	public static void main(final String[] args) {
		final Simple s = new Simple();
		s.interpret("5b");
	}

	int levels = 0;
	final Map<String, Token> registers = new HashMap<String, Token>();
	final Stack<Token> stack = new Stack<Token>();
	int suppress = 0;

	public void interpret(final List<Token> code) {
		for (final Token t : code)
			this.step(t);
	}

	public void interpret(final String code) {

		final Lexer lexer = new Lexer(code);
		Token t;

		while ((t = lexer.next()) != null)
			this.step(t);
		System.out.println(this.stack.pop());
	}

	public void opa() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (a.type == Token.LIST && b.type == Token.CHR) {
			if (a.isString)
				a.string += String.valueOf(b.c);
			a.list.add(b);
			this.stack.push(a);
		} else if (a.type == Token.LIST && b.type != Token.CHR) {
			a.isString = false;
			a.list.add(b);
			this.stack.push(a);
		} else
			throw new RuntimeException("" + a.type + "," + b.type);
	}

	public void opA() {
		final Token a = this.stack.pop();
		if (a.type == Token.INT)
			this.stack.push(new Token(Math.abs(a.i)));
		else if (a.type == Token.DOUBLE)
			this.stack.push(new Token(Math.abs(a.d)));
		else if (a.type == Token.LIST) {
			this.stack.push(a);
			this.interpret(Lexer.staticAll("{A}M"));
		} else
			throw new RuntimeException("" + a.type);
	}

	public void opAND() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (a.type == Token.INT && b.type == Token.INT)
			this.stack.push(new Token(a.i & b.i));
		else if (b.type == Token.LIST || a.type == Token.LIST) {
			this.stack.push(a);
			this.stack.push(b);
			this.interpret(Lexer.staticAll("{&}Z"));
		} else
			throw new RuntimeException("" + a.type + "," + b.type);
	}

	public void opb() {
		final Token a = this.stack.pop();
		final List<Token> result = new ArrayList<Token>();
		result.add(a);
		this.stack.push(new Token(result));
	}

	public void opc() {
		final Token a = this.stack.pop();
		if (a.type == Token.DOUBLE)
			this.stack.push(new Token(Math.ceil(a.d)));
		else if (a.type == Token.LIST) {
			this.stack.push(a);
			this.interpret(Lexer.staticAll("{c}M"));
		} else
			throw new RuntimeException("" + a.type);
	}

	public void opC() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (a.type == Token.LIST && b.type == Token.LIST) {
			if (a.isString && b.isString) {
				a.list.addAll(b.list);
				a.string += b.string;
			} else {
				a.list.addAll(b.list);
				a.isString = false; // not a string anymore
			}
			this.stack.push(a);
		} else
			throw new RuntimeException("" + a.type + "," + b.type);
	}

	public void opDIV() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (b.type == Token.INT && a.type == Token.INT)
			this.stack.push(new Token(a.i / b.i));
		else if (b.type == Token.DOUBLE && a.type == Token.DOUBLE)
			this.stack.push(new Token(a.d / b.d));
		else if (b.type == Token.INT && a.type == Token.DOUBLE)
			this.stack.push(new Token(a.d / b.i));
		else if (b.type == Token.DOUBLE && a.type == Token.INT)
			this.stack.push(new Token(a.i / b.d));
		else if (b.type == Token.LIST || a.type == Token.LIST) {
			this.stack.push(a);
			this.stack.push(b);
			this.interpret(Lexer.staticAll("{/}Z"));
		} else
			throw new RuntimeException(a.type + "," + b.type);
	}

	public void opf() {
		final Token a = this.stack.pop();
		if (a.type == Token.DOUBLE)
			this.stack.push(new Token(Math.floor(a.d)));
		else if (a.type == Token.LIST) {
			this.stack.push(a);
			this.interpret(Lexer.staticAll("{f}M"));
		} else
			throw new RuntimeException("" + a.type);
	}

	public void opH() {
		final Token a = this.stack.pop();

		if (a.type == Token.LIST)
			this.stack.push(a.list.get(0));
		else
			throw new RuntimeException("" + a.type);
	}

	public void opI() {
		final Token a = this.stack.pop();

		if (a.type == Token.LIST) {
			a.list = a.list.subList(0, a.list.size() - 1);
			this.stack.push(a);
		} else
			throw new RuntimeException("" + a.type);
	}

	public void opL() {
		final Token a = this.stack.pop();

		if (a.type == Token.LIST)
			this.stack.push(a.list.get(a.list.size() - 1));
		else
			throw new RuntimeException("" + a.type);
	}

	public void opM() {
		final Token f = this.stack.pop();
		final Token a = this.stack.pop();

		if (f.type == Token.LIST && a.type == Token.LIST) {
			final List<Token> result = new ArrayList<Token>();
			for (final Token token : a.list) {
				this.stack.push(token);
				this.interpret(f.list);
				result.add(this.stack.pop());
			}
			this.stack.push(new Token(result));
		} else
			throw new RuntimeException(a.type + "," + f.type);
	}

	public void opMINUS() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (b.type == Token.INT && a.type == Token.INT)
			this.stack.push(new Token(a.i - b.i));
		else if (b.type == Token.DOUBLE && a.type == Token.DOUBLE)
			this.stack.push(new Token(a.d - b.d));
		else if (b.type == Token.INT && a.type == Token.DOUBLE)
			this.stack.push(new Token(a.d - b.i));
		else if (b.type == Token.DOUBLE && a.type == Token.INT)
			this.stack.push(new Token(a.i - b.d));
		else if (b.type == Token.LIST || a.type == Token.LIST) {
			this.stack.push(a);
			this.stack.push(b);
			this.interpret(Lexer.staticAll("{-}Z"));
		} else
			throw new RuntimeException(a.type + "," + b.type);
	}

	public void opMODULO() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (b.type == Token.INT && a.type == Token.INT)
			this.stack.push(new Token(Math.floorMod(a.i, b.i)));
		else if (b.type == Token.LIST || a.type == Token.LIST) {
			this.stack.push(a);
			this.stack.push(b);
			this.interpret(Lexer.staticAll("{%}Z"));
		} else
			throw new RuntimeException(a.type + "," + b.type);
	}

	public void opMUL() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (b.type == Token.INT && a.type == Token.INT)
			this.stack.push(new Token(a.i * b.i));
		else if (b.type == Token.DOUBLE && a.type == Token.DOUBLE)
			this.stack.push(new Token(a.d * b.d));
		else if (b.type == Token.INT && a.type == Token.DOUBLE)
			this.stack.push(new Token(a.d * b.i));
		else if (b.type == Token.DOUBLE && a.type == Token.INT)
			this.stack.push(new Token(a.i * b.d));
		else if (b.type == Token.LIST || a.type == Token.LIST) {
			this.stack.push(a);
			this.stack.push(b);
			this.interpret(Lexer.staticAll("{*}Z"));
		} else
			throw new RuntimeException(a.type + "," + b.type);
	}

	public void opn() {
		final Token a = this.stack.pop();
		if (a.type == Token.INT)
			this.stack.push(new Token(-1 * Math.abs(a.i)));
		else if (a.type == Token.DOUBLE)
			this.stack.push(new Token(-1.0 * Math.abs(a.d)));
		else if (a.type == Token.LIST) {
			this.stack.push(a);
			this.interpret(Lexer.staticAll("{n}M"));
		} else
			throw new RuntimeException("" + a.type);
	}

	public void opOR() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (a.type == Token.INT && b.type == Token.INT)
			this.stack.push(new Token(a.i | b.i));
		else if (b.type == Token.LIST || a.type == Token.LIST) {
			this.stack.push(a);
			this.stack.push(b);
			this.interpret(Lexer.staticAll("{|}Z"));
		} else
			throw new RuntimeException("" + a.type + "," + b.type);
	}

	public void opPLUS() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (b.type == Token.INT && a.type == Token.INT)
			this.stack.push(new Token(a.i + b.i));
		else if (b.type == Token.DOUBLE && a.type == Token.DOUBLE)
			this.stack.push(new Token(a.d + b.d));
		else if (b.type == Token.INT && a.type == Token.DOUBLE)
			this.stack.push(new Token(a.d + b.i));
		else if (b.type == Token.DOUBLE && a.type == Token.INT)
			this.stack.push(new Token(a.i + b.d));
		else if (b.type == Token.LIST || a.type == Token.LIST) {
			this.stack.push(a);
			this.stack.push(b);
			this.interpret(Lexer.staticAll("{+}Z"));
		} else
			throw new RuntimeException(a.type + "," + b.type);
	}

	public void opPOW() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (b.type == Token.INT && a.type == Token.INT)
			this.stack.push(new Token((int) Math.pow(a.i, b.i)));
		else if (b.type == Token.DOUBLE && a.type == Token.DOUBLE)
			this.stack.push(new Token(Math.pow(a.d, b.d)));
		else if (b.type == Token.INT && a.type == Token.DOUBLE)
			this.stack.push(new Token(Math.pow(a.d, b.i)));
		else if (b.type == Token.DOUBLE && a.type == Token.INT)
			this.stack.push(new Token(Math.pow(a.i, b.d)));
		else if (b.type == Token.LIST || a.type == Token.LIST) {
			this.stack.push(a);
			this.stack.push(b);
			this.interpret(Lexer.staticAll("{^}Z"));
		} else
			throw new RuntimeException(a.type + "," + b.type);
	}

	public void opq() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (b.type == Token.LIST) {
			if (a.isTrue())
				this.interpret(b.list);
		} else
			throw new RuntimeException("" + a.type + "," + b.type);
	}

	public void opR() {
		final Token a = this.stack.pop();
		if (a.type == Token.LIST) {
			Collections.reverse(a.list);
			this.stack.push(a);
		} else if (a.type == Token.INT) {
			final List<Token> result = new ArrayList<Token>();
			for (int i = 0; i <= a.i; i++)
				result.add(new Token(i));
			this.stack.push(new Token(result));
		} else
			throw new RuntimeException("" + a.type);
	}

	public void opS() {
		final Token a = this.stack.pop();
		if (a.type == Token.INT)
			this.stack.push(new Token((int) Math.sqrt(a.i)));
		else if (a.type == Token.DOUBLE)
			this.stack.push(new Token(Math.sqrt(a.d)));
		else if (a.type == Token.LIST) {
			this.stack.push(a);
			this.interpret(Lexer.staticAll("{S}M"));
		} else
			throw new RuntimeException("" + a.type);
	}

	public void opT() {
		final Token a = this.stack.pop();

		if (a.type == Token.LIST) {
			a.list = a.list.subList(1, a.list.size());
			this.stack.push(a);
		} else
			throw new RuntimeException("" + a.type);
	}

	public void opz() {
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (a.type == Token.LIST && b.type == Token.LIST) {
			final List<Token> result = new ArrayList<Token>();
			final int length = Math.min(b.list.size(), a.list.size());
			b.list = b.list.subList(0, length);
			a.list = a.list.subList(0, length);
			for (int i = 0; i < length; i++) {
				final List<Token> temp = new ArrayList<Token>();
				temp.add(a.list.get(i));
				temp.add(b.list.get(i));
				result.add(new Token(temp));
			}
			this.stack.push(new Token(result));
		} else if (a.type == Token.LIST && b.type == Token.INT) {
			final int q = b.i;
			final int length = a.list.size();
			final List<Token> result = new ArrayList<Token>();
			List<Token> temp = new ArrayList<Token>();
			for (int i = 0, f = 1; i < length; i++) {
				temp.add(a.list.get(i));
				if (f % q == 0) {
					result.add(new Token(temp));
					temp = new ArrayList<Token>();
				}
				f++;
			}
			if (temp.size() > 0)
				result.add(new Token(temp));
			this.stack.push(new Token(result));
		}

		else
			throw new RuntimeException("" + a.type + "," + b.type);
	}

	public void opZ() {
		final Token f = this.stack.pop();
		final Token b = this.stack.pop();
		final Token a = this.stack.pop();

		if (f.type == Token.LIST && b.type == Token.LIST
				&& a.type == Token.LIST) {
			final List<Token> result = new ArrayList<Token>();
			final int length = Math.min(b.list.size(), a.list.size());
			b.list = b.list.subList(0, length);
			a.list = a.list.subList(0, length);
			for (int i = 0; i < length; i++) {
				this.stack.push(a.list.get(i));
				this.stack.push(b.list.get(i));
				this.interpret(f.list);
				result.add(this.stack.pop());
			}
			this.stack.push(new Token(result));
		} else if (f.type == Token.LIST && b.type != Token.LIST
				&& a.type == Token.LIST) {
			final List<Token> result = new ArrayList<Token>();
			final int length = a.list.size();
			for (int i = 0; i < length; i++) {
				this.stack.push(a.list.get(i));
				this.stack.push(b);
				this.interpret(f.list);
				result.add(this.stack.pop());
			}
			this.stack.push(new Token(result));
		} else if (f.type == Token.LIST && b.type == Token.LIST
				&& a.type != Token.LIST) {
			final List<Token> result = new ArrayList<Token>();
			final int length = b.list.size();
			for (int i = 0; i < length; i++) {
				this.stack.push(a);
				this.stack.push(b.list.get(i));
				this.interpret(f.list);
				result.add(this.stack.pop());
			}
			this.stack.push(new Token(result));
		} else
			throw new RuntimeException(a.type + "," + b.type + "," + f.type);
	}

	public void step(final Token t) {
		List<Token> tokens;

		Token q;

		if (this.suppress > 0)
			if (t.type == Token.OPR && t.opr == '}')
				this.suppress--;
			else if (t.type == Token.OPR && t.opr == '{') {
				this.suppress++;
				this.levels++;
			}

		if (t.type != Token.OPR)
			this.stack.push(t);
		else if (this.suppress > 0) {
			this.stack.push(t);
			return;
		}

		switch (t.opr) {
		case '[':
			this.stack.push(t);
			break;
		case ']':
			tokens = new ArrayList<Token>();
			while (this.stack.size() > 0) {
				q = this.stack.pop();
				if (q.type != Token.OPR)
					tokens.add(q);
				else if (q.opr != '[')
					tokens.add(q);
				else
					break;
			}
			Collections.reverse(tokens);
			this.stack.push(new Token(tokens));
			break;
		case '{':
			this.stack.push(t);
			this.suppress = 1;
			this.levels = 1;
			break;
		case '}':
			tokens = new ArrayList<Token>();
			while (this.stack.size() > 0) {
				q = this.stack.pop();
				if (q.type != Token.OPR)
					tokens.add(q);
				else if (q.opr != '{')
					tokens.add(q);
				else {
					this.levels--;
					if (this.levels == 0) {
						this.suppress = 0;
						break;
					} else
						tokens.add(q);
				}
			}
			Collections.reverse(tokens);
			this.stack.push(new Token(tokens));
			break;
		case '+':
			this.opPLUS();
			break;
		case '-':
			this.opMINUS();
			break;
		case '/':
			this.opDIV();
			break;
		case '*':
			this.opMUL();
			break;
		case '%':
			this.opMODULO();
			break;
		case '^':
			this.opPOW();
			break;
		case '&':
			this.opAND();
			break;
		case '|':
			this.opOR();
			break;
		case 'A':
			this.opA();
			break;
		case 'C':
			this.opC();
			break;
		case 'H':
			this.opH();
			break;
		case 'I':
			this.opI();
			break;
		case 'L':
			this.opL();
			break;
		case 'M':
			this.opM();
			break;
		case 'R':
			this.opR();
			break;
		case 'S':
			this.opS();
			break;
		case 'T':
			this.opT();
			break;
		case 'Z':
			this.opZ();
			break;
		case 'a':
			this.opa();
			break;
		case 'b':
			this.opb();
			break;
		case 'c':
			this.opc();
			break;
		case 'f':
			this.opf();
			break;
		case 'n':
			this.opn();
			break;
		case 'q':
			this.opq();
			break;
		case 'z':
			this.opz();
			break;
		}
	}
}
